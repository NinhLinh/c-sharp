﻿using System;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.Windows.Forms;

namespace HotelManagementSystem
{
    public partial class ClientScreen : Form
    {
        OleDbConnection conn;
        OleDbDataAdapter adapter;
        DataTable dt;
        DataTable dt1;
        OleDbCommand cmd;
        public ClientScreen()
        {
            InitializeComponent();
            GetData();
        }
        void GetData()
        {
            conn = new OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source= HotelData.accdb");
            dt = new DataTable();
            adapter = new OleDbDataAdapter("SELECT * FROM Client_Data", conn);
            conn.Open();
            adapter.Fill(dt);
            ClientDataView.DataSource = dt;
            dt1 = new DataTable();
            conn.Close();
            adapter = new OleDbDataAdapter("SELECT * FROM Reservation_Data", conn);
            conn.Open();
            adapter.Fill(dt1);
            RoomDataView.DataSource = dt1;
            conn.Close();
            DataView dv = new DataView(dt1);
            ClientRoom.Items.Clear();
            ClientRoom.Text = string.Empty;
            int roomFree = 20;
            for (int i = 100; i < 120; i++)
            {
                dv.RowFilter = string.Format("(RoomID = '{0}') AND (DateOut >= '{1}')", i.ToString(), DateTime.Now.ToShortDateString());
                int idCheck = dv.Count;
                if (idCheck == 0)
                {
                    ClientRoom.Items.Add(i.ToString());
                }
                else
                {
                    roomFree--;
                }
            }
        }
        private void AddBtn_Click(object sender, EventArgs e)
        {
            DataView dv = new DataView(dt);
            dv.RowFilter = string.Format("ClientID = '{0}'", ClientID.Text);
            int count = dv.Count;
            if(count == 0)
            {
                if(ClientID.Text != "ID" && ClientName.Text != "Name")
                {
                    string Query = "INSERT INTO Client_Data(ClientID, ClientName, ClientDOB, ClientPhone, ClientAddress, ClientGender) VALUES (@ClientID, @ClientName, @ClientDOB, @ClientPhone, @ClientAddress, @ClientGender)";
                    cmd = new OleDbCommand(Query, conn);
                    cmd.Parameters.AddWithValue("@ClientID", ClientID.Text);
                    cmd.Parameters.AddWithValue("@ClientName", ClientName.Text);
                    cmd.Parameters.AddWithValue("@ClientDOB", ClientDOB.Value);
                    cmd.Parameters.AddWithValue("@ClientPhone", ClientPhone.Text);
                    cmd.Parameters.AddWithValue("@ClientAddress", ClientAddress.Text);
                    cmd.Parameters.AddWithValue("@ClientGender", ClientGender.Text);
                    conn.Open();
                    cmd.ExecuteNonQuery();
                    conn.Close();
                    MessageBox.Show("Success adding client data!");
                    GetData();
                }
                else
                {
                    MessageBox.Show("Please enter ClientID and Client's name!");
                }
            }
            else
            {
                MessageBox.Show("Client information already exists");
            }
        }

        private void UpdateBtn_Click(object sender, EventArgs e)
        {
            if (ClientID.Text != "ID" && ClientName.Text != "Name")
            {
                string Query = "UPDATE Client_Data SET ClientID = @ClientID, ClientName = @ClientName, ClientDOB = @ClientDOB, ClientPhone = @ClientPhone, ClientAddress = @ClientAddress, ClientGender = @ClientGender WHERE ClientID = @ClientID";
                cmd = new OleDbCommand(Query, conn);
                cmd.Parameters.AddWithValue("@ClientID", ClientID.Text);
                cmd.Parameters.AddWithValue("@ClientName", ClientName.Text);
                cmd.Parameters.AddWithValue("@ClientDOB", ClientDOB.Value);
                cmd.Parameters.AddWithValue("@ClientPhone", ClientPhone.Text);
                cmd.Parameters.AddWithValue("@ClientAddress", ClientAddress.Text);
                cmd.Parameters.AddWithValue("@ClientGender", ClientGender.Text);
                conn.Open();
                cmd.ExecuteNonQuery();
                conn.Close();
                MessageBox.Show("Update Success!");
                GetData();
            }
            else
            {
                MessageBox.Show("Please enter ClientID and Client's name!");
            }
        }

        private void DeleteBtn_Click(object sender, EventArgs e)
        {
            if(ClientID.Text != "ID")
            {
                string Query = "DELETE FROM Client_Data WHERE ClientID = @ClientID";
                cmd = new OleDbCommand(Query, conn);
                cmd.Parameters.AddWithValue("@ClientID", Convert.ToInt32(ClientID.Text));
                conn.Open();
                cmd.ExecuteNonQuery();
                conn.Close();
                MessageBox.Show("Delete success!");
                GetData();
            }
            else
            {
                MessageBox.Show("Please enter Client ID!");
            }
        }

        private void ClientDataView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            ClientID.ForeColor = Color.Black;
            ClientName.ForeColor = Color.Black;
            ClientPhone.ForeColor = Color.Black;
            ClientAddress.ForeColor = Color.Black;
            ClientID.Text = ClientDataView.CurrentRow.Cells[0].Value.ToString();     
            ClientName.Text = ClientDataView.CurrentRow.Cells[1].Value.ToString();
            ClientDOB.Text = ClientDataView.CurrentRow.Cells[2].Value.ToString();
            ClientPhone.Text = ClientDataView.CurrentRow.Cells[3].Value.ToString();
            ClientAddress.Text = ClientDataView.CurrentRow.Cells[4].Value.ToString();
            ClientGender.Text = ClientDataView.CurrentRow.Cells[5].Value.ToString();
        }

        private void ClientID_Enter(object sender, EventArgs e)
        {
            if (ClientID.Text == "ID")
            {
                ClientID.ForeColor = Color.Black;
                ClientID.Text = "";
            }
        }

        private void ClientID_Leave(object sender, EventArgs e)
        {
            if(ClientID.Text == string.Empty)
            {
                ClientID.ForeColor = Color.Gray;
                ClientID.Text = "ID";
            }
        }

        private void ClientName_Enter(object sender, EventArgs e)
        {
            if (ClientName.Text == "Name")
            {
                ClientName.ForeColor = Color.Black;
                ClientName.Text = "";
            }
        }

        private void ClientName_Leave(object sender, EventArgs e)
        {
            if (ClientName.Text == string.Empty)
            {
                ClientName.ForeColor = Color.Gray;
                ClientName.Text = "Name";
            }
        }

        private void ClientPhone_Enter(object sender, EventArgs e)
        {
            if (ClientPhone.Text == "Phone")
            {
                ClientPhone.ForeColor = Color.Black;
                ClientPhone.Text = "";
            }
        }

        private void ClientPhone_Leave(object sender, EventArgs e)
        {
            if (ClientPhone.Text == string.Empty)
            {
                ClientPhone.ForeColor = Color.Gray;
                ClientPhone.Text = "Phone";
            }
        }

        private void ClientAddress_Enter(object sender, EventArgs e)
        {
            if (ClientAddress.Text == "Address")
            {
                ClientAddress.ForeColor = Color.Black;
                ClientAddress.Text = "";
            }
        }

        private void ClientAddress_Leave(object sender, EventArgs e)
        {
            if (ClientAddress.Text == string.Empty)
            {
                ClientAddress.ForeColor = Color.Gray;
                ClientAddress.Text = "Address";
            }
        }

        private void SearchBtn_Click(object sender, EventArgs e)
        {
            string SearchText = SearchCliTextBox.Text;
            DataView dv = new DataView(dt);
            if(SearchCliTextBox.Text == string.Empty || SearchCliTextBox.Text == "Client Search")
            {
                
            }
            else
            {
                dv.RowFilter = string.Format("(ClientID LIKE '%{0}%') or (ClientName LIKE '%{0}%') or (ClientAddress LIKE '%{0}%') or (ClientPhone LIKE '%{0}%') or (ClientGender = '%{0}%')", SearchText);                
            } 
            ClientDataView.DataSource = dv;
        }

        private void ResetBtn_Click(object sender, EventArgs e)
        {
            SearchCliTextBox.ForeColor = Color.Gray;
            SearchCliTextBox.Text = "Client Search";
        }

        private void SearchCliTextBox_Enter(object sender, EventArgs e)
        {
            if(SearchCliTextBox.Text == "Client Search")
            {
                SearchCliTextBox.Text = string.Empty;
                SearchCliTextBox.ForeColor = Color.Black;
            }
        }

        private void SearchCliTextBox_Leave(object sender, EventArgs e)
        {
            if( SearchCliTextBox.Text == string.Empty)
            {
                SearchCliTextBox.ForeColor = Color.Gray;
                SearchCliTextBox.Text = "Client Search";
            }
        }

        private void ResetLeftBtn_Click(object sender, EventArgs e)
        {
            ClientID.ForeColor = Color.Gray;
            ClientName.ForeColor = Color.Gray;
            ClientPhone.ForeColor = Color.Gray;
            ClientAddress.ForeColor = Color.Gray;
            ClientID.Text = "ID";
            ClientName.Text = "Name";
            ClientPhone.Text = "Phone";
            ClientAddress.Text = "Address";
        }
        private void BookBtn_Click(object sender, EventArgs e)
        {
            if(ClientID.Text != "ID" && ClientName.Text != "Name")
            {
                string Query = "INSERT INTO Reservation_Data(ClientID, DateIn, DateOut, RoomID) VALUES (@ClientID, @DateIn, @DateOut, @RoomID)";
                cmd = new OleDbCommand(Query, conn);
                cmd.Parameters.AddWithValue("@ClientID", ClientID.Text);
                cmd.Parameters.AddWithValue("@DateIn", ClientDateIn.Value);
                cmd.Parameters.AddWithValue("@DateOut", ClientDateOut.Value);
                cmd.Parameters.AddWithValue("@RoomID", ClientRoom.Text);
                conn.Open();
                cmd.ExecuteNonQuery();
                conn.Close();
                MessageBox.Show("Success booking rooms!");
                GetData();
            }
            else
            {
                MessageBox.Show("Please enter ClientID and Client's name!");
            }            
        }

        private void CancelRoomBtn_Click(object sender, EventArgs e)
        {
            if (ResIDTextBox.Text != "ResID")
            {
                string Query = "DELETE FROM Reservation_Data WHERE ResID = @ResID";
                cmd = new OleDbCommand(Query, conn);
                cmd.Parameters.AddWithValue("@ResID", Convert.ToInt32(ResIDTextBox.Text));
                conn.Open();
                cmd.ExecuteNonQuery();
                conn.Close();
                MessageBox.Show("Delete success!");
                GetData();
            }
            else
            {
                MessageBox.Show("Please enter the ResID!");
            }
        }

        private void RoomDataView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            ResIDTextBox.Text = RoomDataView.CurrentRow.Cells[0].Value.ToString();
        }

        private void ResIDTextBox_Enter(object sender, EventArgs e)
        {
            if (ResIDTextBox.Text == "ResID")
            {
                ResIDTextBox.Text = string.Empty;
                ResIDTextBox.ForeColor = Color.Black;
            }
        }

        private void ResIDTextBox_Leave(object sender, EventArgs e)
        {
            if (ResIDTextBox.Text == string.Empty)
            {
                ResIDTextBox.ForeColor = Color.Gray;
                ResIDTextBox.Text = "ResID";
            }
        }
    }
}
