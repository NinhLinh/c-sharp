﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using System.Data.OleDb;

namespace HotelManagementSystem
{
    public partial class LoginScreen : Form   //partial: cho phép lưu định nghĩa của 1 class, struct hoặc interface ra nhiều file source code.
    {
        OleDbConnection conn;
        OleDbDataAdapter adapter;
        DataTable dt;
        bool state = true;
        public LoginScreen()
        {
            /* Nó là 1 hàm mặc định do visual studio tạo ra, được định nghĩa trong class LoginScreen ở file LoginScreen.Design.cs. Có tác dụng ghi lại các
            thay đổi khi chúng ta làm việc trên màn hình design của LoginScreen (thay đổi thuộc tính của Form, thêm control….)
            */
            InitializeComponent();
        }
        void GetData()
        {
            conn = new OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source= HotelData.accdb");
            dt = new DataTable();
            adapter = new OleDbDataAdapter("SELECT * FROM Staff_Data", conn);
            conn.Open();
            adapter.Fill(dt);
            conn.Close();
        }

        /*
         * object sender: chính là đối tượng phát sinh ra event. Để sử dụng được nó thì bạn phải ép kiểu lại cho đúng là dùng được.
         * EventArgs e: đối tượng e chứa danh sách các thuộc tính bổ xung khi đối tượng phát sinh event. 
         * Tùy theo đối tượng phát sinh event mà e có các thuộc tính tương ứng.
         */
        private void LogUsername_Enter(object sender, EventArgs e)
        {
            if(LogUsername.Text == "Username")
            {
                LogUsername.Text = string.Empty;  // Khởi tạo text thành chuỗi rỗng
                LogUsername.ForeColor = Color.Black;  //chọn màu cho forecolor (màu nền)
                ErrorNotification.Visible = false;   //it equivalent to calling the Hide(). 
            }
        }

        private void LogPassword_Enter(object sender, EventArgs e)
        {
            if (LogPassword.Text == "Password")
            {
                LogPassword.Text = string.Empty;
                LogPassword.ForeColor = Color.Black;
                ErrorNotification.Visible = false;  //which means it will not be displayed on the screen. It is a way to hide an element in C# code.
            }
        }

        private void LogUsername_Leave(object sender, EventArgs e)
        {
            if (LogUsername.Text == string.Empty)
            {
                LogUsername.Text = "Username";
                LogUsername.ForeColor = Color.Gray;
            }
        }

        private void LogPassword_Leave(object sender, EventArgs e)
        {
            if (LogPassword.Text == string.Empty)
            {
                LogPassword.Text = "Password";
                LogPassword.ForeColor = Color.Gray;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            GetData();

            /*string Username = LogUsername.Text.Trim(); is a way to declare a variable named Username of type string and 
             * assign it the value of LogUsername.Text.Trim() in C# code.
            */
            string Username = LogUsername.Text.Trim();
            string Password = LogPassword.Text.Trim();
            DataView dv = new DataView(dt);

            /*
             * dv.RowFilter = string.Format("(StaffName = '{0}') AND (StaffPassword = '{1}')", Username, Password); 
             * It is a way to filter the rows of a DataView object named dv based on two conditions: 
             * the StaffName column must match the value of Username variable and 
             * the StaffPassword column must match the value of Password variable. 
             */
            dv.RowFilter = string.Format("(StaffName = '{0}') AND (StaffPassword = '{1}')", Username, Password);          
            int check = dv.Count;
            if (check >= 1)
            {
                MainMenu mainMenu = new MainMenu(Username, Password);
                mainMenu.Show();
                this.Hide();
            }
            else
            {
                ErrorNotification.Visible = true;
            }
        }

        private void ShowLoginPassBtn_Click(object sender, EventArgs e)
        {
            if (state == false)
            {
                // it is a way to set the background image of a button named ShowLoginPassBtn to an image resource named _29x29_hide in C# code.
                ShowLoginPassBtn.BackgroundImage = new Bitmap(HotelManagementSystem.Properties.Resources._29x29_hide);
                LogPassword.PasswordChar = '*';
                state = true;
            }
            else
            {
                ShowLoginPassBtn.BackgroundImage = new Bitmap(HotelManagementSystem.Properties.Resources._29x29_show);
                LogPassword.PasswordChar = '\0';
                state = false;
            }
        }
    }
}
