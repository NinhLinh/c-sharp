﻿using System;
using System.Data;
using System.Data.OleDb;
using System.Windows.Forms;

namespace HotelManagementSystem
{
    public partial class ProfileScreen : Form
    {
        //Declare variables
        string UsernameGlobal = string.Empty, PasswordGlobal = string.Empty;
        OleDbConnection conn;
        OleDbDataAdapter adapter;
        DataTable dt;
        OleDbCommand cmd;
        OleDbDataReader reader;
        void GetData()
        {
            conn = new OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=\"C:\\Users\\trg14\\Documents\\Ex myself\\C#.NET\\Final\\HotelData.accdb\"");
            dt = new DataTable();

            // adapter (bộ chuyển đổi) =....: it is a way to create a new object of type OleDbDataAdapter and assign it to a variable named adapter
            adapter = new OleDbDataAdapter("SELECT * FROM Reservation_Data", conn);
            conn.Open();
            adapter.Fill(dt);           
            conn.Close();
        }

        //timer1_Tick : is a way to set the text of a control named Clock to the current date and time
        private void timer1_Tick(object sender, EventArgs e)
        {
            Clock.Text = DateTime.Now.ToString();
        }

        public ProfileScreen(string Name, string Pass)
        {
            InitializeComponent();
            GetData();
            UsernameGlobal = Name;
            PasswordGlobal = Pass;
            Clock.Text = DateTime.Now.ToString();
            cmd = new OleDbCommand("SELECT * FROM Staff_Data WHERE StaffName = @StaffName AND StaffPassword = @StaffPassword", conn);
            cmd.Parameters.AddWithValue("@StaffName", Name);
            cmd.Parameters.AddWithValue("@StaffPassword", Pass);
            conn.Open();

            //reader = ... : is a way to execute a command and build a SqlDataReader object using the command and assign it to a variable named reader
            reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                ProfileID.Text = reader.GetValue(0).ToString();
                ProfileName.Text = Name;
                ProfileDOB.Text = reader.GetValue(2).ToString();
                ProfileGender.Text = reader.GetValue(3).ToString();
                ProfilePhone.Text = reader.GetValue(4).ToString();
                ProfileAddress.Text = reader.GetValue(5).ToString();
            }
            conn.Close();
        }
    }
