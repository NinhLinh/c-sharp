﻿using System;
using System.Data.OleDb;
using System.Data;
using System.Drawing;
using System.Windows.Forms;

namespace HotelManagementSystem
{
    public partial class StaffScreen : Form
    {
        bool state_1 = true, state_2 = true, state = false;
        OleDbConnection conn;
        OleDbDataAdapter adapter;
        DataTable dt;
        OleDbCommand cmd;
        public StaffScreen()
        {
            InitializeComponent();
            GetData();
        }
        string RandomGeneratePass()
        {
            int length = 5;
            const string charSet = "qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM1234567890!@#$%^&*()`~-_=+[]{};:,<.>/?";
            char[] result = new char[length];
            System.Random rand = new System.Random();           
            for(int i = 0; i < length; i++)
            {
                result[i] = charSet[rand.Next(charSet.Length-1)];
            }
            return string.Join(null, result);
        }
        void GetData()
        {
            conn = new OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source= HotelData.accdb");
            dt = new DataTable();
            adapter = new OleDbDataAdapter("SELECT * FROM Staff_Data", conn);
            conn.Open();
            adapter.Fill(dt);
            StaffDataView.DataSource = dt;
            conn.Close();
        }

        private void ResetLeftBtn_Click(object sender, EventArgs e)
        {
            StaffID.ForeColor = Color.Gray;
            StaffName.ForeColor = Color.Gray;
            StaffPhone.ForeColor = Color.Gray;
            StaffAddress.ForeColor = Color.Gray;
            StaffID.Text = "ID";
            StaffName.Text = "Name";
            StaffPhone.Text = "Phone";
            StaffAddress.Text = "Address";
        }

        private void SearchStaffTextBox_Enter(object sender, EventArgs e)
        {
            if(SearchStaffTextBox.Text == "Staff Search")
            {
                SearchStaffTextBox.Text = string.Empty;
                SearchStaffTextBox.ForeColor = Color.Black;
            }
        }

        private void SearchStaffTextBox_Leave(object sender, EventArgs e)
        {
            if (SearchStaffTextBox.Text == string.Empty)
            {
                SearchStaffTextBox.Text = "Staff Search";
                SearchStaffTextBox.ForeColor = Color.Gray;
            }
        }

        private void ResetBtn_Click(object sender, EventArgs e)
        {
            SearchStaffTextBox.ForeColor= Color.Gray;
            SearchStaffTextBox.Text = "Staff Search";
        }

        private void SearchBtn_Click(object sender, EventArgs e)
        {
            string SearchText = SearchStaffTextBox.Text;
            DataView dv = new DataView(dt);
            if (SearchStaffTextBox.Text == string.Empty || SearchStaffTextBox.Text == "Staff Search")
            {

            }
            else
            {
                dv.RowFilter = string.Format("(StaffID LIKE '%{0}%') or (StaffName LIKE '%{0}%') or (StaffAddress LIKE '%{0}%') or (StaffPhone LIKE '%{0}%') or (StaffGender = '%{0}%')", SearchText);
            }
            StaffDataView.DataSource = dv;
        }

        private void ChangePassTextBox_Enter(object sender, EventArgs e)
        {
            if (ChangePassTextBox.Text == "Password")
            {
                ChangePassTextBox.ForeColor = Color.Black;
                ChangePassTextBox.Text = string.Empty;
            }
        }

        private void ChangePassTextBox_Leave(object sender, EventArgs e)
        {
            if (ChangePassTextBox.Text == string.Empty)
            {
                ChangePassTextBox.ForeColor = Color.Gray;
                ChangePassTextBox.Text = "Password";
            }
        }

        private void ChangeRepassTextBox_Enter(object sender, EventArgs e)
        {
            if (ChangeRepassTextBox.Text == "Password")
            {
                ChangeRepassTextBox.ForeColor = Color.Black;
                ChangeRepassTextBox.Text = string.Empty;
                ErrorSecondTypePass.Visible = false;
            }
        }

        private void ChangeRepassTextBox_Leave(object sender, EventArgs e)
        {
            if (ChangeRepassTextBox.Text == string.Empty)
            {
                ChangeRepassTextBox.ForeColor = Color.Gray;
                ChangeRepassTextBox.Text = "Password";
                ErrorSecondTypePass.Visible = false;
            }
        }

        private void ApplyNewPassBtn_Click(object sender, EventArgs e)
        {
            bool check;
            if(ChangePassTextBox.Text == ChangeRepassTextBox.Text)
            {
                check = true;
            }
            else
            {
                ErrorSecondTypePass.Visible = true;
                check= false;
                ChangeRepassTextBox.Text = "Password";
            }
            if(check == true)
            {
                if (StaffID.Text != "ID")
                {
                    string Query = "UPDATE Staff_Data SET StaffID = @StaffID, StaffName = @StaffName, StaffDOB = @StaffDOB, StaffGender = @StaffGender, StaffPhone = @StaffPhone, StaffAddress = @StaffAddress, StaffPassword = @StaffPassword WHERE StaffID = @StaffID";
                    cmd = new OleDbCommand(Query, conn);
                    cmd.Parameters.AddWithValue("@StaffID", StaffID.Text);
                    cmd.Parameters.AddWithValue("@StaffName", StaffName.Text);
                    cmd.Parameters.AddWithValue("@StaffDOB", StaffDOB.Value);
                    cmd.Parameters.AddWithValue("@StaffGender", StaffGender.Text);
                    cmd.Parameters.AddWithValue("@StaffPhone", StaffPhone.Text);
                    cmd.Parameters.AddWithValue("@StaffAddress", StaffAddress.Text);
                    cmd.Parameters.AddWithValue("@StaffPassword", ChangeRepassTextBox.Text);
                    conn.Open();
                    cmd.ExecuteNonQuery();
                    conn.Close();
                    MessageBox.Show("Success updating Staff data!");
                    GetData();
                }
                else
                {
                    MessageBox.Show("Please enter StaffID and Staff's name!");
                }
            }
        }

        private void ShowPassBtn_Click(object sender, EventArgs e)
        {
            if(state_1 == false)
            {
                ShowPassBtn.BackgroundImage = new Bitmap(@"C:\Users\trg14\Documents\Ex myself\C#.NET\Final\HotelManagementSystem\Resources\29x29_hide.png");
                ChangePassTextBox.PasswordChar = '*';
                state_1 = true;
            }
            else
            {
                ShowPassBtn.BackgroundImage = new Bitmap(@"C:\Users\trg14\Documents\Ex myself\C#.NET\Final\HotelManagementSystem\Resources\29x29_show.png");
                ChangePassTextBox.PasswordChar = '\0';
                state_1 = false;
            }
        }

        private void ShowRepassBtn_Click(object sender, EventArgs e)
        {
            if (state_2 == false)
            {
                ShowRepassBtn.BackgroundImage = new Bitmap(HotelManagementSystem.Properties.Resources._29x29_hide);
                ChangeRepassTextBox.PasswordChar = '*';
                state_2 = true;
            }
            else
            {
                ShowRepassBtn.BackgroundImage = new Bitmap(HotelManagementSystem.Properties.Resources._29x29_show);
                ChangeRepassTextBox.PasswordChar = '\0';
                state_2 = false;
            }
        }

        private void StaffID_Enter(object sender, EventArgs e)
        {
            if(StaffID.Text == "ID")
            {
                StaffID.Text = string.Empty;
                StaffID.ForeColor = Color.Black;
            }
        }

        private void StaffID_Leave(object sender, EventArgs e)
        {
            if (StaffID.Text == string.Empty)
            {
                StaffID.Text = "ID";
                StaffID.ForeColor = Color.Gray;
            }
        }

        private void StaffName_Enter(object sender, EventArgs e)
        {
            if (StaffName.Text == "Name")
            {
                StaffName.Text = string.Empty;
                StaffName.ForeColor = Color.Black;
            }
        }

        private void StaffName_Leave(object sender, EventArgs e)
        {
            if (StaffName.Text == string.Empty)
            {
                StaffName.Text = "Name";
                StaffName.ForeColor = Color.Gray;
            }
        }

        private void StaffPhone_Enter(object sender, EventArgs e)
        {
            if (StaffPhone.Text == "Phone")
            {
                StaffPhone.Text = string.Empty;
                StaffPhone.ForeColor = Color.Black;
            }
        }

        private void StaffPhone_Leave(object sender, EventArgs e)
        {
            if (StaffPhone.Text == string.Empty)
            {
                StaffPhone.Text = "Phone";
                StaffPhone.ForeColor = Color.Gray;
            }
        }

        private void StaffAddress_Enter(object sender, EventArgs e)
        {
            if (StaffAddress.Text == "Address")
            {
                StaffAddress.Text = string.Empty;
                StaffAddress.ForeColor = Color.Black;
            }
        }

        private void StaffAddress_Leave(object sender, EventArgs e)
        {
            if (StaffAddress.Text == string.Empty)
            {
                StaffAddress.Text = "Address";
                StaffAddress.ForeColor = Color.Gray;
            }
        }

        private void AddStaffBtn_Click(object sender, EventArgs e)
        {
            DataView dv = new DataView(dt);
            dv.RowFilter = string.Format("StaffID = '{0}'", StaffID.Text);
            int count = dv.Count;
            if (count == 0)
            {
                if (StaffID.Text != "ID" && StaffName.Text != "Name")
                {
                    string Query = "INSERT INTO Staff_Data(StaffID, StaffName, StaffDOB, StaffGender, StaffPhone, StaffAddress, StaffPassword) VALUES (@StaffID, @StaffName, @StaffDOB, @StaffGender, @StaffPhone, @StaffAddress, @StaffPassword)";
                    cmd = new OleDbCommand(Query, conn);
                    cmd.Parameters.AddWithValue("@StaffID", StaffID.Text);
                    cmd.Parameters.AddWithValue("@StaffName", StaffName.Text);
                    cmd.Parameters.AddWithValue("@StaffDOB", StaffDOB.Value);
                    cmd.Parameters.AddWithValue("@StaffGender", StaffGender.Text);
                    cmd.Parameters.AddWithValue("@StaffPhone", StaffPhone.Text);
                    cmd.Parameters.AddWithValue("@StaffAddress", StaffAddress.Text);
                    cmd.Parameters.AddWithValue("@StaffPassword", RandomGeneratePass());
                    conn.Open();
                    cmd.ExecuteNonQuery();
                    conn.Close();
                    MessageBox.Show("Success adding client data!");
                    GetData();
                }
                else
                {
                    MessageBox.Show("Please enter StaffID and Staff's name!");
                }
            }
            else
            {
                MessageBox.Show("Staff information already exists");
            }
        }

        private void DeleteStaffBtn_Click(object sender, EventArgs e)
        {
            if (StaffID.Text != "ID")
            {
                string Query = "DELETE FROM Staff_Data WHERE StaffID = @StaffID";
                cmd = new OleDbCommand(Query, conn);
                cmd.Parameters.AddWithValue("@StaffID", Convert.ToInt32(StaffID.Text));
                conn.Open();
                cmd.ExecuteNonQuery();
                conn.Close();
                MessageBox.Show("Delete success!");
                GetData();
            }
            else
            {
                MessageBox.Show("Please enter StaffID!");
            }
        }

        private void StaffDataView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            StaffID.ForeColor = Color.Black;
            StaffName.ForeColor = Color.Black;
            StaffPhone.ForeColor = Color.Black;
            StaffAddress.ForeColor = Color.Black;
            StaffID.Text = StaffDataView.CurrentRow.Cells[0].Value.ToString();
            StaffName.Text = StaffDataView.CurrentRow.Cells[1].Value.ToString();
            StaffDOB.Text = StaffDataView.CurrentRow.Cells[2].Value.ToString();
            StaffPhone.Text = StaffDataView.CurrentRow.Cells[4].Value.ToString();
            StaffAddress.Text = StaffDataView.CurrentRow.Cells[5].Value.ToString();
            StaffGender.Text = StaffDataView.CurrentRow.Cells[3].Value.ToString();
        }   

        private void UpdateStaffBtn_Click_1(object sender, EventArgs e)
        {
            if (StaffID.Text != "ID" && StaffName.Text != "Name")
            {
                string Query = "UPDATE Staff_Data SET StaffID = @StaffID, StaffName = @StaffName, StaffDOB = @StaffDOB, StaffGender = @StaffGender, StaffPhone = @StaffPhone, StaffAddress = @StaffAddress WHERE StaffID = @StaffID";
                cmd = new OleDbCommand(Query, conn);
                cmd.Parameters.AddWithValue("@StaffID", StaffID.Text);
                cmd.Parameters.AddWithValue("@StaffName", StaffName.Text);
                cmd.Parameters.AddWithValue("@StaffDOB", StaffDOB.Value);
                cmd.Parameters.AddWithValue("@StaffGender", StaffGender.Text);
                cmd.Parameters.AddWithValue("@StaffPhone", StaffPhone.Text);
                cmd.Parameters.AddWithValue("@StaffAddress", StaffAddress.Text);
                conn.Open();
                cmd.ExecuteNonQuery();
                conn.Close();
                MessageBox.Show("Success updating Staff data!");
                GetData();
            }
            else
            {
                MessageBox.Show("Please enter StaffID and Staff's name!");
            }
        }

        private void ChangePasswordBtn_Click_1(object sender, EventArgs e)
        {
            if(state == true)
            {
                ChangePassMiniPanel.Visible = false;
                state = false;
            }
            else
            {
                ChangePassMiniPanel.Visible=true;
                state = true;
            }
        }
    }
}
