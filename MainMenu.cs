﻿using System;
using System.Windows.Forms;

namespace HotelManagementSystem
{
    public partial class MainMenu : Form   //partial: cho phép lưu định nghĩa của 1 class, struct hoặc interface ra nhiều file source code.
    {
        //Declare variable
        Form CurrentForm = null;  //is a way to declare a variable named CurrentForm of type Form and assign it the value of null in C# code.
        ProfileScreen profileScreen;
        RoomManagementScreen roomManagementScreen;
        StaffScreen staffScreen;
        ClientScreen clientScreen;
        string GlobalUsername, GlobalPassword;
        public MainMenu(string Username, string Password)
        {
            InitializeComponent();
            UserProfileMiniName.Text = Username;
            GlobalPassword = Password;
            GlobalUsername = Username;

            //create a new object of type StaffScreen and assign it to a variable named staffScreen.
            staffScreen = new StaffScreen() { Dock = DockStyle.Fill, TopLevel = false, TopMost = true }; 
            profileScreen = new ProfileScreen(Username, Password) { Dock = DockStyle.Fill, TopLevel = false, TopMost = true };
            roomManagementScreen = new RoomManagementScreen() { Dock = DockStyle.Fill, TopLevel = false, TopMost = true };
            clientScreen = new ClientScreen() {  Dock = DockStyle.Fill, TopLevel = false, TopMost= true };
        }
        private void OpenChildForm(Form ChildForm, object ActiveButton)
        {
            if (CurrentForm != null)
            {
                this.ShowChildScreen.Controls.Remove(CurrentForm);
                CurrentForm = null;
            }
                this.ShowChildScreen.Controls.Add(ChildForm);
                CurrentForm = ChildForm;
                ChildForm.Show();
        }
        private void MenuScr_Click(object sender, EventArgs e)
        {
            if(LeftControlPanel.Width == 259)
            {
                LeftControlPanel.Width = 55;
                ProfileScrBtn.Width = 37;
                ProfileScrBtn.Text = string.Empty;
                RoomSrcBtn.Width = 37;
                RoomSrcBtn.Text = string.Empty;
                StaffScrBtn.Width = 37;
                StaffScrBtn.Text = string.Empty;
                ClientManagScrBtn.Width = 37;
                ClientManagScrBtn.Text = string.Empty;
            }
            else
            {
                LeftControlPanel.Width = 259;
                ProfileScrBtn.Width = 240;
                ProfileScrBtn.Text = "Profile";
                RoomSrcBtn.Width = 240;
                RoomSrcBtn.Text = "Room Management";

                StaffScrBtn.Width = 240;
                StaffScrBtn.Text = "Satistic";
                ClientManagScrBtn.Width = 240;
                ClientManagScrBtn.Text = "Client Information";
            }
        }

        private void Exit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void ProfileScrBtn_Click(object sender, EventArgs e)
        {
            // is a way to call a custom method named OpenChildForm with two parameters: profileScreen and sender
            OpenChildForm(profileScreen, sender);
        }

        private void RoomSrcBtn_Click(object sender, EventArgs e)
        {
            OpenChildForm(roomManagementScreen, sender);
        }

        private void StaffScrBtn_Click(object sender, EventArgs e)
        {
            OpenChildForm(staffScreen, sender);
        }
        private void ClientManagScrBtn_Click(object sender, EventArgs e)
        {
            OpenChildForm(clientScreen, sender);
        }
    }
}
