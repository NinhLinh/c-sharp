﻿using System;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.Windows.Forms;

namespace HotelManagementSystem
{
    public partial class RoomManagementScreen : Form
    {
        int freeRoom = 20;
        OleDbConnection conn;
        OleDbDataAdapter adapter;
        DataTable dt;
        void GetData()
        {
            conn = new OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source= HotelData.accdb");
            dt = new DataTable();
            adapter = new OleDbDataAdapter("SELECT * FROM Reservation_Data", conn);
            conn.Open();
            adapter.Fill(dt);
            conn.Close();
            DataView dv = new DataView(dt);
            for (int i = 100; i < 120; i++)
            {
                dv.RowFilter = string.Format("(RoomID = '{0}') AND (DateOut >= '{1}')", i.ToString(), DateTime.Now.ToShortDateString());
                int idCheck = dv.Count;
                if (idCheck == 1)
                {
                    freeRoom--;
                    ((PictureBox)this.RoomPanel.Controls["Room" + i.ToString()]).BackgroundImage = new Bitmap(HotelManagementSystem.Properties.Resources.OFFLock);
                }
                else
                {
                    ((PictureBox)this.RoomPanel.Controls["Room" + i.ToString()]).BackgroundImage = new Bitmap(HotelManagementSystem.Properties.Resources.OKKey);
                }
            }
            if(freeRoom > 0)
            {
                FreeRoomCount.ForeColor = Color.MediumSeaGreen;
                FreeRoomTitle.ForeColor = Color.MediumSeaGreen;
            }
            else
            {
                FreeRoomCount.ForeColor = Color.OrangeRed;
                FreeRoomTitle.ForeColor = Color.OrangeRed;
            }
            FreeRoomCount.Text = freeRoom.ToString();
        }
        public RoomManagementScreen()
        {
            InitializeComponent();
            GetData();
        }
    }
}
